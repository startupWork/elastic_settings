#!/bin/bash

IP=localhost:9200

SETTING_FILE=settings.json

GROUPE_NAME=publications_inria

MAPPING_FILE=publications.json

curl -XPUT $IP/$GROUPE_NAME

curl -XPOST $IP/$GROUPE_NAME/_close

curl -XPUT $IP/$GROUPE_NAME/_settings -H 'Content-Type: application/json' -d @$SETTING_FILE

curl -XPUT $IP/$GROUPE_NAME/_mapping -H 'Content-Type: application/json' -d @$MAPPING_FILE

curl -XPOST $IP/$GROUPE_NAME/_open

GROUPE_NAME=nested_researchteam_inria

MAPPING_FILE=nested_researchteam.json

curl -XPUT $IP/$GROUPE_NAME

curl -XPOST $IP/$GROUPE_NAME/_close

curl -XPUT $IP/$GROUPE_NAME/_settings -H 'Content-Type: application/json' -d @$SETTING_FILE

curl -XPUT $IP/$GROUPE_NAME/_mapping -H 'Content-Type: application/json' -d @$MAPPING_FILE

curl -XPOST $IP/$GROUPE_NAME/_open

GROUPE_NAME=nested_inria_author

MAPPING_FILE=nested_inria_authors.json

curl -XPUT $IP/$GROUPE_NAME

curl -XPOST $IP/$GROUPE_NAME/_close

curl -XPUT $IP/$GROUPE_NAME/_settings -H 'Content-Type: application/json' -d @$SETTING_FILE

curl -XPUT $IP/$GROUPE_NAME/_mapping -H 'Content-Type: application/json' -d @$MAPPING_FILE

curl -XPOST $IP/$GROUPE_NAME/_open
